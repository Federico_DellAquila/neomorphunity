﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LegSlotPivot : MonoBehaviour
{
    public LegSlot[] GetLegSlots()
    {
        int ChildNum = transform.childCount;
        Debug.Assert(ChildNum > 0, this.name + " do not have any LegSlots");

        LegSlot[] LegSlotsList = new LegSlot[ChildNum];
        for (int i = 0; i < ChildNum; ++i)
        {
            LegSlotsList[i] = transform.GetChild(i).GetComponent<LegSlot>();
        }
        return LegSlotsList;
    }
}