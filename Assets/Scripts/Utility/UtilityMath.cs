﻿using UnityEngine;
public static class UtilityMath
{
    // Since I can't find a better way to represent an infinite ray using Debug.DrawLine, here's my bad solution.
    public const float MAX_RAY_MAGNITUDE = 999999.9f;

    // Return a normalized Unit Direction Vector3 between two points
   public static Vector3 GetUnitDirection(in Vector3 From, in Vector3 To, in bool DrawDebug = false, in Color DrawDebugColor = default, in float Duration = 0.0f)
    {
        Vector3 UnitDirection = (To - From).normalized;
        if (DrawDebug == true)
        {
            Debug.DrawRay(From, UnitDirection, DrawDebugColor, Duration);
        }
        return UnitDirection;
    }
    // Return a Quaternion representing the Rotation between two points
    public static Quaternion FindLookAtRotation(in Vector3 From, in Vector3 To, in bool DrawDebug = false, in Color DrawDebugColor = default, in float Duration = 0.0f)
    {
        Vector3 Direction = GetUnitDirection(From, To, DrawDebug, DrawDebugColor, Duration);
        return Quaternion.LookRotation(Direction);
    }

    public static bool LineTrace(out RaycastHit Hit, in Vector3 StartPoint, in Vector3 Direction, in float Distance = MAX_RAY_MAGNITUDE, 
        in bool DrawDebug = false, in Color HitColor = default, in Color MissColor = default, in float Duration = 0.0f)
    {
        bool HasHit = Physics.Raycast(StartPoint, Direction, out Hit, Distance);
        if (DrawDebug == true)
        {
            // Since I can't for the life of me find a way to set Green and Red as
            // default Color parameters here's how I'm going to do it for now
            Color SelectedHitColor = (HitColor == default) ? Color.green : HitColor;
            Color SelectedMissColor = (MissColor == default) ? Color.red : HitColor;
            float HitDistance = HasHit ? Hit.distance : Distance;

            Color RayColor = HasHit ? SelectedHitColor : SelectedMissColor;
            Debug.DrawRay(StartPoint, Direction * HitDistance, RayColor, Duration);
        }
        return HasHit;
    }
}
