﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LegSlot : MonoBehaviour
{
    [ReadOnly]
    public Leg AssociatedLeg;

    // Start is called before the first frame update
    void Start()
    {
        AssociatedLeg = GetComponentInChildren<Leg>();
        Debug.Assert(AssociatedLeg != null, this.name + " doesn't have an AssociatedLeg assigned");
        AssociatedLeg.AssociatedLegSlot = this;
    }
    public void DetachFromChild()
    {
        Debug.Assert(AssociatedLeg != null, this.name + " doesn't have an AssociatedLeg assigned");
        AssociatedLeg.DetachFromParent();
    }

    public bool ShouldMove(in Vector3 HitLocation)
    {
        Debug.Assert(AssociatedLeg != null, this.name + " doesn't have an AssociatedLeg assigned");
        float Distance = Vector3.Distance(AssociatedLeg.transform.position, HitLocation);
        return Distance > AssociatedLeg.Threshold;
    }

    public void CalculateLegOffsetLocationAndRotation(in Vector3 HitLocation)
    {
        Vector3 StartPoint = this.transform.position;
        Vector3 Direction = UtilityMath.GetUnitDirection(StartPoint, AssociatedLeg.OffsetMoveDirection(HitLocation));

        RaycastHit Hit;
        bool HasHit = UtilityMath.LineTrace(out Hit, StartPoint, Direction);
        if (HasHit == true)
        {
            AssociatedLeg.SetLocationAndOrientation(Hit.point, UtilityMath.FindLookAtRotation(Hit.point, Hit.point + Hit.normal));
        }
    }

    public bool TraceDown(out Vector3 HitLocation, out Vector3 HitNormal)
    {
        RaycastHit Hit;
        Vector3 StartPoint = this.transform.position;
        Vector3 Direction = this.transform.up * -1.0f;

        bool HasHit = UtilityMath.LineTrace(out Hit, StartPoint, Direction, UtilityMath.MAX_RAY_MAGNITUDE, true);
        HitLocation = Hit.point;
        HitNormal = Hit.normal;
        return HasHit;
    }
}
