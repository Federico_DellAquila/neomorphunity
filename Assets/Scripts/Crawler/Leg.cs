﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Leg : MonoBehaviour
{
    [ReadOnly]
    public LegSlot AssociatedLegSlot;

    [ReadOnly]
    public float Threshold;
    public float MinThreshold = 0.7f;
    public float MaxThreshold = 1.1f;
    public void SetLocationAndOrientation(in Vector3 Location, in Quaternion Orientation)
    {
        transform.localPosition = Location;
        transform.localRotation = Orientation;
        RandomizeThreshold();
    }

    public void DetachFromParent()
    {
        this.transform.parent = null;
    }

    public Vector3 OffsetMoveDirection(in Vector3 Location)
    {
        Vector3 LegToHitDirection = UtilityMath.GetUnitDirection(this.transform.position, Location);
        Vector3 LegToHitDistanceVector = Location + LegToHitDirection * RandomDistanceOffset();
        return LegToHitDistanceVector;
    }

    private void RandomizeThreshold()
    {
        Threshold = Random.Range(MinThreshold, MaxThreshold);
    }

    private float RandomDistanceOffset()
    {
        return Random.Range(MinThreshold * 0.5f, MaxThreshold * 0.5f);
    }

    // Start is called before the first frame update
    void Start()
    {
        RandomizeThreshold();
    }

    // Update is called once per frame
    void Update()
    {
    }
}
