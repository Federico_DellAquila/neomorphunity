﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Body : MonoBehaviour
{
    [ReadOnly]
    public LegSlot[] LegSlotsList;

    // Start is called before the first frame update
    void Start()
    {
        LegSlotPivot lsp = transform.GetComponentInChildren<LegSlotPivot>();
        Debug.Assert(lsp != null, "No child posses the LegSlotPivot component");

        LegSlotsList = lsp.GetLegSlots();
        foreach (LegSlot ls in LegSlotsList)
        {
            // We detach Legs from parent to make their position and rotation independent
            ls.DetachFromChild();

            // Set starting position of the Legs, ignoring Threshold
            Vector3 HitLocation;
            Vector3 HitNormal;
            bool bHit = ls.TraceDown(out HitLocation, out HitNormal);
            if (bHit == true)
            {
                Quaternion LookRotation = UtilityMath.FindLookAtRotation(HitLocation, HitLocation + HitNormal, false);
                ls.AssociatedLeg.transform.SetPositionAndRotation(HitLocation, LookRotation);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Assert(LegSlotsList.Length > 0, this.name + " do not have any LegSlots");
        foreach (LegSlot ls in LegSlotsList) 
        {
            Leg al = ls.AssociatedLeg;
            Debug.Assert(al != null, ls.name + " do not have an Associated Leg");

            Vector3 HitLocation;
            Vector3 HitNormal;
            bool bHit = ls.TraceDown(out HitLocation, out HitNormal);
            if (bHit == true)
            {


                if (ls.ShouldMove(HitLocation))
                {
                    ls.CalculateLegOffsetLocationAndRotation(HitLocation);
                }
            }
        }
    }
}
