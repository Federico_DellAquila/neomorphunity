﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControls : MonoBehaviour
{
    public float Speed;

    public Transform PlayerMesh;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Cursor.lockState = CursorLockMode.Locked;

        Rigidbody rb = GetComponent<Rigidbody>();

        float HorizontaInput = Input.GetAxis("Horizontal");
        float VerticalInput = Input.GetAxis("Vertical");

        Vector3 Forward = Camera.main.transform.forward;
        Vector3 Right = Camera.main.transform.right;

        Forward.y = 0;
        Right.y = 0;

        Forward.Normalize();
        Right.Normalize();

        Vector3 MoveDirection = Forward * VerticalInput + Right * HorizontaInput;
        rb.velocity = new Vector3(MoveDirection.x * Speed, 0, MoveDirection.z * Speed);

        if (MoveDirection != Vector3.zero)
        {
            PlayerMesh.rotation = Quaternion.LookRotation(MoveDirection);
        }
    }

    private void Update()
    {
        
    }
}
