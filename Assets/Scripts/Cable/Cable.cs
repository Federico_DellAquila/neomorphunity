﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

[ExecuteInEditMode]
public class Cable : MonoBehaviour
{
    private LineRenderer LineRendererComponent;
    public GameObject CableFrom;
    public GameObject CableTo;

    // Start is called before the first frame update
    void Start()
    {
        // Collect LineRender Component
        LineRendererComponent = GetComponent<LineRenderer>();

        Debug.Assert(LineRendererComponent != null, "Missing LineRenderer component");
        Debug.Assert(CableFrom != null, "Missing CableFrom target");
        Debug.Assert(CableTo != null, "Missing CableTo target");
    }

    // Update is called once per frame
    void LateUpdate()
    {
        LineRendererComponent.SetPosition(0, CableFrom.transform.position);
        LineRendererComponent.SetPosition(1, CableTo.transform.position);
    }
}
